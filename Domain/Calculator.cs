﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2;
        }
        public int Substraction(int value1, int value2)
        {
            return value1 - value2;
        }
        public int Multiplication(int value1, int value2)
        {
            return value1 * value2;
        }
        public double Division(double value1, double value2)
        {
            return value1 / value2;
        }


    }
}
