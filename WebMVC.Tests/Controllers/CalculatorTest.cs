﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }



 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        //Resta
        public void Substraction()
        {
            //Arrange (Organizar)
            int value1 = 15;
            int value2 = 9;
            int expected = 6;

            //Act (Actuar)

            int actual = SystemUnderTest.Substraction(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }
        [TestMethod]
        //Multiplicacion
        public void Multiplication()
        {
            //Arrange (Organizar)
            int value1 = 90;
            int value2 = 8;
            int expected = 720;

            //Act (Actuar)

            int actual = SystemUnderTest.Multiplication(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }
        [TestMethod]
        //Division
        public void Division()
        {
            //Arrange (Organizar)
            double value1 = 1600;
            double value2 = 32;
            double expected = 50;

            //Act (Actuar)

            double actual = SystemUnderTest.Division(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<double>(expected, actual, "Error, valores no coinciden");

        }
    }
}
